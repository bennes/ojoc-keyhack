#!/bin/bash

GENERAL_APK=com.tellm.android.app-*.apk

if [ ! -d data ]; then
	mkdir data;
fi
if [ ! -f data/$GENERAL_APK ]; then
	./download_apk.sh
	mv $GENERAL_APK data/;
fi
if [ ! -f data/libhmac.so ]; then
	unzip -p data/$GENERAL_APK lib/x86/libhmac.so > data/libhmac.so
fi

# Find the address of the function Java_*_init
address=$(nm -D data/libhmac.so | grep Java | grep init | awk '{print $1}' | sed 's/^[0]*//')

# Decompile, remove leading address, remove first opcode byte (c6 or c7) and sort
lines=$(objdump -D data/libhmac.so | grep -A 21 $address | grep -A 16 "c6 80" | sed 's/^\s*[0-9a-f]*:\s*c[67]\s*80\s*//' | sort | awk '{print $5" "$6" "$7" "$8}' | sed 's/mov.*//')

# Reformat in C array style
formatted=$(echo -e "0x$lines" | tr -s '\n' ' ' | sed 's/ /,0x/g')

# Delete leading comma and spaces
length=$(echo "$formatted" | wc -c | awk '{print $1}')
let length=$length-4
formatted=$(echo "$formatted" | head -c $length)

# Create the .c source
cat > tmp.c << EOF
#include <stdint.h>
#include <stdio.h>
#include "decrypt.h"

int main (void) {
   uint8_t v[] = { $formatted };
   return decode(v);
}
EOF

# Compile, run and delete
gcc tmp.c decrypt.c -o tmp
rm tmp.c
key=$(./tmp)
if [ $? -eq 255 ]; then
	echo "Key not found"
else
	echo "Key found: $key"
fi
rm tmp