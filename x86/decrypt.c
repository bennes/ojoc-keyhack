#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <assert.h>
#include "decrypt.h"

/** Shuffles around the SIGNATURE to obtain an encryption key
 *
 * @param encryptionKey[] array where the encryption key shall be stored
 */
static void GenerateEncryptionKey(char encryptionKey[CRYPTTABLE_SIZE]) {
    char const *sig = SIGNATURE;
    int signatureLength = strlen(sig);
    uint8_t shuffleCounter = 0;

    assert(encryptionKey != NULL);
    for (int i = 0; i < CRYPTTABLE_SIZE; i++)
        encryptionKey[i] = i;
    
    for (int shuffleIndex = 0; shuffleIndex < CRYPTTABLE_SIZE; ++shuffleIndex) {
        uint8_t encryptionKeyByte = encryptionKey[shuffleIndex];
        shuffleCounter += sig[shuffleIndex % signatureLength];
        shuffleCounter += encryptionKeyByte;
        encryptionKey[shuffleIndex] = encryptionKey[shuffleCounter];
        encryptionKey[shuffleCounter] = encryptionKeyByte;
    }
}

/** Perform the actual decoding algorithm
 *  The algorithm mutates the key during deconding
 *  
 *  @param xorKey[] Byte Array with the actual key read out from the
 *                  library .so file
 */
int decode (const uint8_t xorKey[CLIENT_SECRET_SIZE])
{
    char encryptionKey[CRYPTTABLE_SIZE];
    char clientSecret[CLIENT_SECRET_SIZE+1];
    uint8_t secretCounter = 0;

    GenerateEncryptionKey(encryptionKey);

    for (int secretIndex = 0; secretIndex < CLIENT_SECRET_SIZE; ++secretIndex) {
        uint8_t encryptionKeyByte = encryptionKey[secretIndex + 1];
        secretCounter += encryptionKeyByte;
        encryptionKey[secretIndex + 1] = encryptionKey[secretCounter];
        encryptionKey[secretCounter] = encryptionKeyByte;
        clientSecret[secretIndex] = (uint8_t)(xorKey[secretIndex] ^ encryptionKey[(uint8_t)(encryptionKey[secretIndex + 1] + encryptionKeyByte)]);
        if (isalpha(clientSecret[secretIndex]) == 0) {
            // We assume from the history of keys that a valid key only
            // contains printable characters
            return -1;
        }
    }
    clientSecret[CLIENT_SECRET_SIZE] = 0;
#if OUTPUT_KEYBYTES
    for (int i = 0; i < CLIENT_SECRET_SIZE; i++) {
        printf("%#02x",xorKey[i]);
        putchar((i == CLIENT_SECRET_SIZE-1) ? '\n': ' ');
    }
#endif
    (void) fwrite(clientSecret,CLIENT_SECRET_SIZE,1,stdout);
    (void) putchar('\n');
    return 0;
}

