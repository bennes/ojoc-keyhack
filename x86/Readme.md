## x86 HMAC Key Retrieval ##

Jodel hides the API key in the `libhmac.so` in the Android app.

The tools in this directory try to retrieve the key from this library.

### Getting Started ###

A Linux environment with `wget`, `gcc`, and `unzip` is assumed.

1. Run
    `$ ./all.sh`
